using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("Flag Stats")]
    public bool hasFlag = true;
    public bool flagPlaced;

    public bool gamePaused;

    public static GameManager instance;


    void Awake()
    {
        // Set the instance to this script
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        // Flag bools
        hasFlag = false;
        flagPlaced = false;

        // Set time to real time
        Time.timeScale = 1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if(flagPlaced)
        {
            Debug.Log("SWeave has placed the flag!");
        }

        // Cancel button is "escape", pauses game
        if(Input.GetButtonDown("Cancel"))
            TogglePauseGame();
    }

    public void TogglePauseGame()
    {
        gamePaused = !gamePaused;
        Time.timeScale = gamePaused == true ? 0.0f : 1.0f;

        // If gamePaused is not true, cursor not locked, if true, cursor is locked.
        Cursor.lockState = gamePaused == true ? CursorLockMode.None : CursorLockMode.Locked;
    }

    public void PlaceFlag()
    {
        flagPlaced = true;
    }
}
