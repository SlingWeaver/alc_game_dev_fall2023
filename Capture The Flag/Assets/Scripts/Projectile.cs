using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [Header("Projectile")]
    public GameObject projectile;
    public Transform firePoint;
    public float projectileSpeed;
    private bool isPlayer;

    [Header("Shoot Rate & Time")]
    public float shootRate;
    private float lastShootTime;

    [Header("Ammo")]
    public int curAmmo;
    public int maxAmmo;
    public bool infiniteAmmo;

    // Runs before start
    void Awake()
    {
        // Are we attached to the player
        if (GetComponent<FirstPersonMovement>())
        {
            isPlayer = true;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool CanShoot()
    {
        if (Time.time - lastShootTime >= shootRate)
        {
            if (curAmmo > 0 || infiniteAmmo)
                return true;
        }
        return false;
    }

    public void Shoot()
    {
        // Cool down & reduce ammo
        lastShootTime = Time.time;
        curAmmo--;

        // Spawn/Instantiate projectile
        GameObject projectileObject = Instantiate(projectile, firePoint.position, firePoint.rotation);

        // Set the velocity of the projectile
        projectileObject.GetComponent<Rigidbody>().velocity = firePoint.forward * projectileSpeed;
    }
}
