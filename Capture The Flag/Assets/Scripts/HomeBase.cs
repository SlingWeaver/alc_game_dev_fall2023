using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeBase : MonoBehaviour
{

    private GameManager gm;
    public Renderer flagRend;

    public Renderer flagStickRend;

    // Start is called before the first frame update
    void Start()
    {
        // Get GameManager component, and FlagRenderer component
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        flagRend = GameObject.Find("FriendlyFlagFlag").GetComponent<Renderer>();
        flagStickRend = GameObject.Find("FriendlyFlagStick").GetComponent<Renderer>();

        // Hide PlayerBase flag
        flagRend.enabled = false;
        flagStickRend.enabled = false;
    }

    void OnTriggerEnter(Collider other)
    {
        // Must have the tag Player, whilst holding enemy flag
        if(other.CompareTag("Player") && gm.hasFlag)
        {
            Debug.Log("SWeave has claimed the flag!");

            // Show PlayerBase flag
            flagRend.enabled = true;
            flagStickRend.enabled = true;
        }
    }
}
