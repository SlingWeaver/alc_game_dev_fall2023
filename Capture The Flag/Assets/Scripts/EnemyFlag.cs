using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFlag : MonoBehaviour
{
    private GameManager gm;
    private Renderer flagRend;
    private Renderer flagStickRend;

    // Start is called before the first frame update
    void Start()
    {
         // Get GameManager component, and FlagRenderer component
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        flagRend = GameObject.Find("EnemyFlagFlag").GetComponent<Renderer>();
        flagStickRend = GameObject.Find("EnemyFlagStick").GetComponent<Renderer>();

        // Hide PlayerBase flag
        flagRend.enabled = true;
        flagStickRend.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
         if(other.CompareTag("Player"))
        {
            gm.hasFlag = true;
            flagRend.enabled = false;
            flagStickRend.enabled = false;
        }
    }
}
