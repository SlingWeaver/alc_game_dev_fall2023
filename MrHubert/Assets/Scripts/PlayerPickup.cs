using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPickup : MonoBehaviour
{
    public Transform handPosition; // The position where the object will be held
    public PlayerRaycast playerRaycast; // The PlayerRaycast component

    private GameObject heldObject = null; // The object currently being held

    public FireExtinguisherFunction fireExtinguisher;

    public string pickupTag = "Pickup";

    void Update() // Calls every frame
    {
        if (heldObject == null) // If there is no object being held
        {
            RaycastHit? hit = playerRaycast.Raycast(); // Detect raycast
            if (hit.HasValue && hit.Value.transform.gameObject.CompareTag(pickupTag)) // Detect raycast against objects with tag
            {

                fireExtinguisher.pickupToggle.enabled = true;
                if (Input.GetKeyDown(KeyCode.E)) // When the player presses 'E'
                {
                    fireExtinguisher.pickupToggle.enabled = false;
                    Pickup(hit.Value.transform.gameObject); // Call pickup method 
                }
            }
            else
            {
                fireExtinguisher.pickupToggle.enabled = false;
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.E)) // When the player presses E while holding on object
            {
                Drop(); // Call drop method
            }

            fireExtinguisher.pickupToggle.enabled = false;
        }
    }

    void Pickup(GameObject pickupObject)
    {
        heldObject = pickupObject;
        Rigidbody rb = heldObject.GetComponent<Rigidbody>();
        if (rb != null)
        {
            rb.isKinematic = true;
        }
        heldObject.transform.position = handPosition.position;
        heldObject.transform.rotation = GameObject.Find("Player").transform.rotation; // Add this line
        heldObject.transform.parent = handPosition;
    }


    void Drop()
    {
        Rigidbody rb = heldObject.GetComponent<Rigidbody>();
        if (rb != null)
        {
            rb.isKinematic = false;
        }
        heldObject.transform.parent = null;
        heldObject = null;
    }
}
