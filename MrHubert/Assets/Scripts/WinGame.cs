using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class WinGame : MonoBehaviour
{

    public int finishedHomework = 0;

    public SoundEffects soundEffects;
    public AudioSource audioSource;

    public bool isPlaying = false;

    void Update()
    {
        

        if (finishedHomework == 5)
        {  
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);    
        }
    }

    void FixedUpdate()
    {

        if (isPlaying == false)
        {
            if (SceneManager.GetSceneByName("win").isLoaded)
            {
                audioSource.PlayOneShot(soundEffects.win);
                isPlaying = true;
            }
            
        }
    }
}
