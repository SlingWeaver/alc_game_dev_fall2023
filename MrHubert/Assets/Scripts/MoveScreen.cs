using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveScreen : MonoBehaviour
{
    public Transform screen;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = screen.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
