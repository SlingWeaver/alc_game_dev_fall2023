using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VentInteraction : MonoBehaviour
{
    public float interactionDistance = 5f; // The maximum distance the player can interact from
    public HubertTeleport hubertTeleport; // Reference to the HubertTeleport script

    public GameObject ventPlaceholder; // Reference to the placeholder object
    public GameObject ventRotation; // Reference to the object containing the new rotation

    public AudioSource audioSource; // Reference to Player Audio Source

    public SoundEffects soundEffects; // Reference to SoundEffects script

    public bool isPlaying = false;

   void FixedUpdate()
{
    if (hubertTeleport != null && hubertTeleport.currentLocationIndex == 0) // If Huberts currentLocationIndex is equal to 0
{
    // Cast a ray from the player's position in the direction they are looking
    Ray ray = new Ray(transform.position, transform.forward); // Cast raw forward
    RaycastHit hit; // Create hit

    if (Physics.Raycast(ray, out hit, interactionDistance)) // If the raycast touches something within the interaction distance
    {
        // Check if the ray hit an object with the "Garage" tag
        if (hit.collider.CompareTag("Vent"))
        {
            isPlaying = false;
            // Restart Hubert's teleport timer
            hubertTeleport.PlayerReacted();
            Debug.Log("Player reacted!");

            // Stop animation
            hubertTeleport.ventOpen.GetComponent<Animator>().enabled = false;
            // Reset rotation of vent
            ventRotation.transform.rotation = ventPlaceholder.transform.rotation;

            // // Play interaction sound effect

            if (isPlaying == false)
            {
                InteractAudio();
            }
        }
    }

    void InteractAudio()
{
    if (isPlaying == false)
    {
        audioSource.PlayOneShot(soundEffects.interacted);
        isPlaying = true;
        StartCoroutine(CheckIfAudioFinished());
    }       
}

IEnumerator CheckIfAudioFinished()
{
    while (audioSource.isPlaying)
    {
        yield return new WaitForSeconds(0.1f);
    }
    isPlaying = false;
}
}
    }        
}
