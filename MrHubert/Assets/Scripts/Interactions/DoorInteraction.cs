using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorInteraction : MonoBehaviour
{
    public float interactionDistance = 5f; // The maximum distance the player can interact from
    public HubertTeleport hubertTeleport; // Reference to the HubertTeleport script
    public GameObject doorRotation; // Reference to the new rotation object
    public GameObject doorPlaceholder; // Reference to the placeholder for door position

    public SoundEffects soundEffects; // Reference to the SoundEffects script
    public AudioSource audioSource; // Reference to Player Audio Source

    public bool isPlaying = false;

   void FixedUpdate()
{
    if (hubertTeleport != null && hubertTeleport.currentLocationIndex == 2) // If Huberts current index is equal to 2
{
    // Cast a ray from the player's position in the direction they are looking
    Ray ray = new Ray(transform.position, transform.forward); 
    RaycastHit hit;

    if (Physics.Raycast(ray, out hit, interactionDistance)) // If the raycast touches something within the interaction distance
    {
        // Check if the ray hit an object with the "Garage" tag
        if (hit.collider.CompareTag("Door"))
        {
            // Restart Hubert's teleport timer
            hubertTeleport.PlayerReacted();
            Debug.Log("Player reacted!");

            hubertTeleport.audioDoorOpen.GetComponent<Animator>().enabled = false; // Stop animation
            doorRotation.transform.rotation = doorPlaceholder.transform.rotation; // Set new rotation
            
            if (isPlaying == false)
            {
                InteractAudio();
            }

        }
    }

    void InteractAudio()
{
    if (isPlaying == false)
    {
        audioSource.PlayOneShot(soundEffects.interacted);
        isPlaying = true;
        StartCoroutine(CheckIfAudioFinished());
    }       
}

IEnumerator CheckIfAudioFinished()
{
    while (audioSource.isPlaying)
    {
        yield return new WaitForSeconds(0.1f);
    }
    isPlaying = false;
}
}
    }        
}
