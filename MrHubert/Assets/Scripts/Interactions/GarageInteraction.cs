using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarageInteraction : MonoBehaviour
{
    public float interactionDistance = 5f; // The maximum distance the player can interact from
    public HubertTeleport hubertTeleport; // Reference to the HubertTeleport script

    public GameObject garageRotation; // Reference to object with the new rotation
    public GameObject garagePlaceholder; // Reference to the placeholder for position

    public AudioSource audioSource; // Reference to Player Audio Source

    public SoundEffects soundEffects; // Reference to SoundEffects script

    public bool isPlaying = false;

   void Update()
{
    if (hubertTeleport != null && hubertTeleport.currentLocationIndex == 1) // If huberts current index is equal to 1
{
    // Cast a ray from the player's position in the direction they are looking
    Ray ray = new Ray(transform.position, transform.forward); 
    RaycastHit hit;

    if (Physics.Raycast(ray, out hit, interactionDistance)) // If the raycast touches something within the interaction distance
    {
        // Check if the ray hit an object with the "Garage" tag
        if (hit.collider.CompareTag("Garage"))
        {
            // Restart Hubert's teleport timer
            hubertTeleport.PlayerReacted();
            Debug.Log("Player reacted!");
            hubertTeleport.garageOpen.GetComponent<Animator>().enabled = false; // Disable animation

            garageRotation.transform.position = garagePlaceholder.transform.position; // Reset position

            // Play interaction sound effect
            // audioSource.PlayOneShot(soundEffects.interacted);

            if (isPlaying == false)
            {
                InteractAudio();
            }
        }
    }

   void InteractAudio()
{
    if (isPlaying == false)
    {
        audioSource.PlayOneShot(soundEffects.interacted);
        isPlaying = true;
        StartCoroutine(CheckIfAudioFinished());
    }       
}

IEnumerator CheckIfAudioFinished()
{
    while (audioSource.isPlaying)
    {
        yield return new WaitForSeconds(0.1f);
    }
    isPlaying = false;
}
}
    }        
}

