using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class EndGame : MonoBehaviour
{

    public HubertTeleport hubertTeleport;

    void Update()
    {
        if (hubertTeleport.gameOver == true)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

}

