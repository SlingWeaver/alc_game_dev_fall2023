using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class EssayButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public bool isHeld = false;
    
    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("Held");
        isHeld = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Debug.Log("Not Held");
        isHeld = false;
    }
}
