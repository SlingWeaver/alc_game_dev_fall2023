using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public BellworkButton holdButton;
    public UnityEngine.UI.Image bellworkTimer;
    public float waitTime = 30.0f;

    public TextMeshProUGUI homeworkCounter;

    public WinGame winGame;

    void Update()
    {
        if (holdButton.isHeld == true)
        {
            Debug.Log("Works!");
            // Fill button over 5 seconds
            bellworkTimer.fillAmount += 1f / waitTime * Time.deltaTime;

            if (bellworkTimer.fillAmount >= 1f) // Check if timer is completely filled up
            {
                winGame.finishedHomework += 1;
                ScoreUI();
                Debug.Log(winGame.finishedHomework);

                holdButton.GetComponent<Button>().interactable = false;
    

                // // Reset fillAmount and isHeld for the next round
                bellworkTimer.fillAmount = 0f;
                holdButton.isHeld = false;
            }
        }
    }

    void ScoreUI()
    {
        homeworkCounter.text = "Homework: "+ winGame.finishedHomework;
    }
}


