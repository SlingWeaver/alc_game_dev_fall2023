using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HubertTeleport : MonoBehaviour
{
    public GameObject hubertObject; // The object to teleport



    public GameObject hubertSkin;

    public Transform[] positions; // The possible positions
    public AudioClip audioClip; // The audio clip
    public float minTime = 5f; // The minimum time to wait
    public float maxTime = 25f; // The maximum time to wait

    public AudioSource audioSource; // The audio source
    public bool shouldTeleport = true; // Whether Hubert should teleport

    public GameObject audioDoorOpen; // Door Open

    public GameObject ventOpen; // Vent Open

    public GameObject garageOpen; // Garage Open

    public AudioClip garageOpenAudio; // Garage opening audio
    public AudioClip doorOpenAudio; // Door opening audio
    public AudioClip ventOpenAudio; // Vent opening audio

    public int currentLocationIndex = -1; // current location index variable

    public bool gameOver = false; // check if the game is over or not

    public float reactionTimer = 0f; // Reaction timer to end the game

    void Start()
    {
        // Get the audio source component
        // audioSource = GetComponent<AudioSource>();

        // Set the audio clip
        audioSource.clip = audioClip;

        // Play the audio clip
        audioSource.Play();

        // Start the teleporting coroutine after the audio clip finishes
        StartCoroutine(TeleportAfterAudio());

        audioDoorOpen.GetComponent<Animator>().enabled = false; // Set the animation to false

        ventOpen.GetComponent<Animator>().enabled = false; // Set the animation to false

        garageOpen.GetComponent<Animator>().enabled = false; // Set the animation to false

    }

    IEnumerator TeleportAfterAudio() // IEnumerator is a coroutine that will allow for the teleporting to start after the tutorial audio plays
    {
        // Wait for the audio clip to finish playing
        yield return new WaitForSeconds(audioClip.length); // Tells the coroutine to wait a certain amount of seconds, the seconds being equal to the length of the audioClip

        while (true)
        {
            if (shouldTeleport)
            {
                reactionTimer = 0f;

                // Wait for a random amount of time
                yield return new WaitForSeconds(Random.Range(minTime, maxTime)); // If shouldTeleport == true it will wait for a random amount of time before teleporting

                // Teleport hubertObject to a random position
                currentLocationIndex = Random.Range(0, positions.Length); // It can teleport randomly between the several different positions
               hubertObject.transform.position = positions[currentLocationIndex].position; // Hubert teleports to whatever object was chosen, the chosen object was stored in the currentLocationIndex

                // Set Hubert's rotation to match the teleport position's rotation
                hubertObject.transform.rotation = positions[currentLocationIndex].rotation;

                // Check if Hubert is in the third location
                if (currentLocationIndex == 2) 
                {
                    
                    PlayAudioAnimation();
                }
                // Check if Hubert is in the first location
                if (currentLocationIndex == 0)
                {
                    PlayVentAnimation();
                }
                // Check if Hubert is in the second location
                if (currentLocationIndex == 1)
                {
                    PlayGarageAnimation();
                }

                // Stop teleporting until the player reacts
                shouldTeleport = false;

                // If Hubert is stopped at a location he will be visible
                if (shouldTeleport == false)
                {
                    hubertSkin.GetComponent<SkinnedMeshRenderer>().enabled = true;
                }
            }
            // If Hubert is at a location the reactionTimer beings
            else if (!shouldTeleport)
            {
                reactionTimer += Time.deltaTime;
                
                // If the player does not react in 6 seconds the game ends
                if (reactionTimer >= 6f)
                {
                    gameOver = true;
                }
            }

            yield return null; // Tells the coroutine to pause until the next frame or until shouldTeleport resumes true
        }
    }

    void PlayAudioAnimation()
    {
        // Add code here to play your animation
        audioDoorOpen.GetComponent<Animator>().enabled = true;
        audioSource.PlayOneShot(doorOpenAudio);
    }

    void PlayVentAnimation()
    {
        ventOpen.GetComponent<Animator>().enabled = true;
        audioSource.PlayOneShot(ventOpenAudio);
    }

    void PlayGarageAnimation()
    {
        garageOpen.GetComponent<Animator>().enabled = true;
        audioSource.PlayOneShot(garageOpenAudio);
        
    }

    public void PlayerReacted()
    {
        // Resume teleporting when the player reacts
        shouldTeleport = true;
       

        reactionTimer = 0f;

        if (shouldTeleport == true)
        {
            hubertSkin.GetComponent<SkinnedMeshRenderer>().enabled = false;
        }
    }
}
