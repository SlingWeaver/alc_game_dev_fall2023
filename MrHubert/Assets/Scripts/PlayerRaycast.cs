using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRaycast : MonoBehaviour
{
    public float range = 2f;

  

    // public Camera fpsCam;

    // Update is called once per frame
    void Update()
    {
       Raycast();
    }

    public RaycastHit? Raycast()
    {
        RaycastHit hit;

        Vector3 screenCenter = new Vector3(Screen.width / 2, Screen.height / 2, 0);

        Ray ray = Camera.main.ScreenPointToRay(screenCenter);

        if (Physics.Raycast(ray, out hit, range))
        {
            Debug.DrawRay(ray.origin, ray.direction, Color.red);

            return hit;
        }

        return null;
    }
}
