using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public float mouseSensitivity = 180f; // Mouse sensitivity for both axis

    public Transform playerBody; // A reference to the player object

    float xRotation = 0f; // Variable for the xRotation of the camera

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; // Locks cursor on start
        Cursor.visible = false; // Sets cursor to invisible on start
    }

    void FixedUpdate()
    {
        CameraFunctionality();
    }

    void CameraFunctionality()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime; // Creates mouseX variable, assigns horizontal movement to it
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime; // Creates mouseY variable, assigns vertical movement to it

        xRotation -= mouseY; // Camera math
        xRotation = Mathf.Clamp(xRotation, -90f, 90f); // Locks the vertical movement to 180 degrees up and down

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f); // Rotates player body when mouse is moved horizontally
        playerBody.Rotate(Vector3.up * mouseX); // Rotates the camera NOT player body, when mouse is moved vertically
    }
}
