using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    public Transform groundCheck;

    public float moveSpeed = 12f;
    public float gravity = -9.81f;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    Vector3 velocity;
    bool isGrounded;

    public AudioSource audioSource;

    public AudioClip footsteps;
    
    void Update()
    {  
        MovementPhysics();
        GroundStuff();          
    }

    void FixedUpdate()
    {
        BasicMovement();
    }

    void BasicMovement()
    {
        float x = Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime; // Creates variable "x" and assigns left and right movement
        float z = Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime; // Creates variable "z" and assigns forward and backwards movement

        /* Takes the direction the player is facing and looks "right", 
        takes the direction the player is facing and looks "forward", 
        allows the player to move relative to itself 
        regardless of worldspace coordinates */
  
        Vector3 moveDirection = transform.right * x + transform.forward * z;  

        controller.Move(moveDirection * moveSpeed * Time.deltaTime);

        // If the player is moving and the footsteps sound is not already playing, play the sound
    if ((x != 0 || z != 0) && !audioSource.isPlaying)
    {
        audioSource.clip = footsteps;
        audioSource.Play();
    }
    // If the player is not moving, stop the sound
    else if (x == 0 && z == 0)
    {
        audioSource.Pause();
    }
        
    }

    void MovementPhysics()
    {
        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }

    void GroundStuff()
    {
       isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

       if (isGrounded && velocity.y < 0)
       {
            velocity.y = -2f;
       }
    }
}
