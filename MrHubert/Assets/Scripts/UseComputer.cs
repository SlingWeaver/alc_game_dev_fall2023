using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;

public class UseComputer : MonoBehaviour
{
    public PlayerMovement playerMovement; // Reference to the PlayerMovement script
    public MouseLook mouseLook; // Reference to the MouseLook script

    public bool turnOnComputer = false; // Bool to check if the player has activated the computer
    public bool hasPressedE = false; // Bool to check if the player has pressed E
    public bool hasExited = false; // Bool to check if the player has pressed E after the initial E press

    public PlayerRaycast playerRaycast; // A reference to the PlayerRaycast script

    public TextMeshProUGUI computerToggle; // A reference to the computerToggle TMP

    public GameObject worldspaceComputerCanvas; // Reference to the computer screen canvas

    public string computerTag = "MainComputer"; // Reference to the "MainComputer" tag on the MainComputer object

    void Start()
    {
        computerToggle.enabled = false; // TMP set to false on start
        turnOnComputer = false; // Computer has not been activated by the player on start
        hasExited = false; // The player has not pressed E after initial E press on start
        worldspaceComputerCanvas.SetActive(false);
    }

    void Update()
    {
        RaycastDetectComputer(); // Call RaycastDetectComputer method
        exitComputerUI(); // Call exitComputerUI method
    }

    void RaycastDetectComputer()
    {
        RaycastHit? hit = playerRaycast.Raycast(); // new variable calling the Raycast method & RaycastHit from PlayerRaycast. ? Means if it doesn't return a hit, it will return null.

        if (hit.HasValue && hit.Value.transform.CompareTag(computerTag)) // check if ray has made contact with the computerTag
        {
           if (!hasPressedE) // If hasPressedE is false and the player is looking at the MainComputer
           {
                worldspaceComputerCanvas.SetActive(false);
                computerToggle.enabled = true; // Turn on computer UI appears

            if (Input.GetKeyDown(KeyCode.E)) // If player presses E while looking at the MainComputer
            {
                computerToggle.enabled = false; // comptuer UI disappears
                turnOnComputer = true; // Computer is now active by the player
                playerMovement.audioSource.Pause();
                hasPressedE = true; // Player has now pressed the E key 
                UsingComputerFunctionality(); // Calls UsingComputerFunctionality method
            }
           }
           
        }
        else // If the player is not viewing the MainComputer
           {
            computerToggle.enabled = false; // UI disappears once again
           }
    }
    
    void exitComputerUI() // Method checking if the player is wanting to exit the computer
    {
        if (turnOnComputer == true && hasExited == true) // Check if hasExited and turnOnComputer is true
        {
            if (Input.GetKeyDown(KeyCode.E)) // If the player presses E while the above is true
            {
                ClosingComputerFunctionality(); // Run ClosingComputerFunctionality method
                hasPressedE = false; // hasPressedE becomes false
                turnOnComputer = false; // Computer has now been disabled by the player
                hasExited = false; // Reset to false
            }
        }
        else if (turnOnComputer == true) // If turnOnComputer is true but hasExited is false
        {
            if (Input.GetKeyDown(KeyCode.E)) // If player presses E while using the computer and hasExited is not already true
            {
                hasExited = true; // Set hasExited to true
            }
        }
    }

    void UsingComputerFunctionality() // Method that runs when player has activated the computer
    {
        Cursor.lockState = CursorLockMode.None; // Unlocks cursor for the player
        Cursor.visible = true; // Cursor becomes visible allowing interaction with the UI
        playerMovement.enabled = false; // PlayerMovement disables
        mouseLook.enabled = false; // MouseLook disables
        worldspaceComputerCanvas.SetActive(true);
    }

    void ClosingComputerFunctionality() // Method that runs when the player has exited the computer
    {
        Cursor.lockState = CursorLockMode.Locked; // Cursor re-locks
        Cursor.visible = false; // Cursor becomes invisible once again
        playerMovement.enabled = true; // Enable player movement when closing the computer
        mouseLook.enabled = true; // Enable MouseLook when closing the computer
        worldspaceComputerCanvas.SetActive(false);
    }
}
