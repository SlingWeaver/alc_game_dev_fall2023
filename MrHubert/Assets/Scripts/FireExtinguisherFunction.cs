using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FireExtinguisherFunction : MonoBehaviour
{
   public Transform myHands;
   public bool isHeld;
   public ParticleSystem extinguisherSmoke;

   public TextMeshProUGUI pickupToggle; // A reference to the computerToggle TMP

    void Start()
    {
        isHeld = false;
        extinguisherSmoke.Stop();
        pickupToggle.enabled = false; // TMP set to false on start
    }

    void Update()
    {
        
        float distance = Vector3.Distance(gameObject.transform.position, myHands.transform.position);
        if (distance < 0.01f) // Replace 0.01 with a suitable threshold
        {
            isHeld = true;

            if (isHeld == true)
            {
                if (Input.GetMouseButton(0))
                {
                    Debug.Log("Spray!");
                    if (!extinguisherSmoke.isPlaying)
                    {
                        extinguisherSmoke.Play();
                    }
                }
                else if (Input.GetMouseButtonUp(0))
                {
                    extinguisherSmoke.Stop();
                }
            }
        }
        else
        {
            isHeld = false;
            if (extinguisherSmoke.isPlaying)
            {
                extinguisherSmoke.Stop();
            }
        }
    }
}
